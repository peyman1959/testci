using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class temsg : MonoBehaviour
{
    public TMP_Text Text;
    private int counter;

    public void Add()
    {
        counter++;
        Text.text = counter.ToString();
    }
}
